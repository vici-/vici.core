(ns vici.ui.controls-test
  (:require [criterium.core :as crit]
            [clojure.string :as str]
            [vici.ui.controls :as v-c]))


(defn text+style->ui [text style]
  (merge style {:text text}))

;; Will investigate further
(defn iter-loop-reduce-match-style [txt stl]
  (let [stl-iter (clojure.lang.RT/iter stl)
        txt-iter (clojure.lang.RT/iter txt)]
    (loop [product []]
      (if (and (.hasNext stl-iter) (.hasNext txt-iter))
        (recur (concat product
                       (conj (last (reduce (fn [prod style]
                                      (let [[current rest] (map (partial apply str) (split-at (:size style) (first prod)))]
                                        [rest (conj (last prod) (text+style->ui current style))]))
                                    [(.next txt-iter) []] (.next stl-iter)))
                            {:text "\n"})))
        product))))

(defn mapcat-reduce-match-style [txt stl]
  (mapcat (fn [text-line style-line]
         (->> style-line
              (reduce (fn [prod style]
                   (let [[current rest] (map (partial apply str) (split-at (:size style) (first prod)))]
                     [rest (conj (last prod) (text+style->ui current style))])) [text-line []] )
              last
              (#(conj % {:text "\n"}))))
       txt stl))



(def style-grid
  [[{:size 5 :fill "alert-fg" :background "selection-background-bg"} {:size 2 :fill "accent-2-fg"}]
   [{:size 2}
    {:size 20 :fill "accent-fg"}
    {:size 2}
    {:size 1 :fill "extra-fg"}
    {:size 6 :fill "detail-fg"}
    {:size 1}]
   [{:size 2}
    {:size 8 :fill "accent-fg"}
    {:size 2}
    {:size 4 :fill "extra-fg"}
    {:size 10 :fill "symbols-fg"}
    {:size 1}]
   [{:size 1 :fill "accent-2-fg"}]
   []
   [{:size 5 :fill "alert-fg" :background "selection-background-bg"} {:size 2 :fill "accent-2-fg"}]
   [{:size 2}
    {:size 20 :fill "accent-fg"}
    {:size 2}
    {:size 1 :fill "extra-fg"}
    {:size 6 :fill "detail-fg"}
    {:size 1}]
   [{:size 2}
    {:size 8 :fill "accent-fg"}
    {:size 2}
    {:size 4 :fill "extra-fg"}
    {:size 10 :fill "symbols-fg"}
    {:size 1}]
   [{:size 1 :fill "accent-2-fg"}]
   [][{:size 5 :fill "alert-fg" :background "selection-background-bg"} {:size 2 :fill "accent-2-fg"}]
   [{:size 2}
    {:size 20 :fill "accent-fg"}
    {:size 2}
    {:size 1 :fill "extra-fg"}
    {:size 6 :fill "detail-fg"}
    {:size 1}]
   [{:size 2}
    {:size 8 :fill "accent-fg"}
    {:size 2}
    {:size 4 :fill "extra-fg"}
    {:size 10 :fill "symbols-fg"}
    {:size 1}]
   [{:size 1 :fill "accent-2-fg"}]
   [][{:size 5 :fill "alert-fg" :background "selection-background-bg"} {:size 2 :fill "accent-2-fg"}]
   [{:size 2}
    {:size 20 :fill "accent-fg"}
    {:size 2}
    {:size 1 :fill "extra-fg"}
    {:size 6 :fill "detail-fg"}
    {:size 1}]
   [{:size 2}
    {:size 8 :fill "accent-fg"}
    {:size 2}
    {:size 4 :fill "extra-fg"}
    {:size 10 :fill "symbols-fg"}
    {:size 1}]
   [{:size 1 :fill "accent-2-fg"}]
   [][{:size 5 :fill "alert-fg" :background "selection-background-bg"} {:size 2 :fill "accent-2-fg"}]
   [{:size 2}
    {:size 20 :fill "accent-fg"}
    {:size 2}
    {:size 1 :fill "extra-fg"}
    {:size 6 :fill "detail-fg"}
    {:size 1}]
   [{:size 2}
    {:size 8 :fill "accent-fg"}
    {:size 2}
    {:size 4 :fill "extra-fg"}
    {:size 10 :fill "symbols-fg"}
    {:size 1}]
   [{:size 1 :fill "accent-2-fg"}]
   [][{:size 5 :fill "alert-fg" :background "selection-background-bg"} {:size 2 :fill "accent-2-fg"}]
   [{:size 2}
    {:size 20 :fill "accent-fg"}
    {:size 2}
    {:size 1 :fill "extra-fg"}
    {:size 6 :fill "detail-fg"}
    {:size 1}]
   [{:size 2}
    {:size 8 :fill "accent-fg"}
    {:size 2}
    {:size 4 :fill "extra-fg"}
    {:size 10 :fill "symbols-fg"}
    {:size 1}]
   [{:size 1 :fill "accent-2-fg"}]
   [][{:size 5 :fill "alert-fg" :background "selection-background-bg"} {:size 2 :fill "accent-2-fg"}]
   [{:size 2}
    {:size 20 :fill "accent-fg"}
    {:size 2}
    {:size 1 :fill "extra-fg"}
    {:size 6 :fill "detail-fg"}
    {:size 1}]
   [{:size 2}
    {:size 8 :fill "accent-fg"}
    {:size 2}
    {:size 4 :fill "extra-fg"}
    {:size 10 :fill "symbols-fg"}
    {:size 1}]
   [{:size 1 :fill "accent-2-fg"}]
   [][{:size 5 :fill "alert-fg" :background "selection-background-bg"} {:size 2 :fill "accent-2-fg"}]
   [{:size 2}
    {:size 20 :fill "accent-fg"}
    {:size 2}
    {:size 1 :fill "extra-fg"}
    {:size 6 :fill "detail-fg"}
    {:size 1}]
   [{:size 2}
    {:size 8 :fill "accent-fg"}
    {:size 2}
    {:size 4 :fill "extra-fg"}
    {:size 10 :fill "symbols-fg"}
    {:size 1}]
   [{:size 1 :fill "accent-2-fg"}]
   [][{:size 5 :fill "alert-fg" :background "selection-background-bg"} {:size 2 :fill "accent-2-fg"}]
   [{:size 2}
    {:size 20 :fill "accent-fg"}
    {:size 2}
    {:size 1 :fill "extra-fg"}
    {:size 6 :fill "detail-fg"}
    {:size 1}]
   [{:size 2}
    {:size 8 :fill "accent-fg"}
    {:size 2}
    {:size 4 :fill "extra-fg"}
    {:size 10 :fill "symbols-fg"}
    {:size 1}]
   [{:size 1 :fill "accent-2-fg"}]
   [][{:size 5 :fill "alert-fg" :background "selection-background-bg"} {:size 2 :fill "accent-2-fg"}]
   [{:size 2}
    {:size 20 :fill "accent-fg"}
    {:size 2}
    {:size 1 :fill "extra-fg"}
    {:size 6 :fill "detail-fg"}
    {:size 1}]
   [{:size 2}
    {:size 8 :fill "accent-fg"}
    {:size 2}
    {:size 4 :fill "extra-fg"}
    {:size 10 :fill "symbols-fg"}
    {:size 1}]
   [{:size 1 :fill "accent-2-fg"}]
   [][{:size 5 :fill "alert-fg" :background "selection-background-bg"} {:size 2 :fill "accent-2-fg"}]
   [{:size 2}
    {:size 20 :fill "accent-fg"}
    {:size 2}
    {:size 1 :fill "extra-fg"}
    {:size 6 :fill "detail-fg"}
    {:size 1}]
   [{:size 2}
    {:size 8 :fill "accent-fg"}
    {:size 2}
    {:size 4 :fill "extra-fg"}
    {:size 10 :fill "symbols-fg"}
    {:size 1}]
   [{:size 1 :fill "accent-2-fg"}]
   [][{:size 5 :fill "alert-fg" :background "selection-background-bg"} {:size 2 :fill "accent-2-fg"}]
   [{:size 2}
    {:size 20 :fill "accent-fg"}
    {:size 2}
    {:size 1 :fill "extra-fg"}
    {:size 6 :fill "detail-fg"}
    {:size 1}]
   [{:size 2}
    {:size 8 :fill "accent-fg"}
    {:size 2}
    {:size 4 :fill "extra-fg"}
    {:size 10 :fill "symbols-fg"}
    {:size 1}]
   [{:size 1 :fill "accent-2-fg"}]
   [][{:size 5 :fill "alert-fg" :background "selection-background-bg"} {:size 2 :fill "accent-2-fg"}]
   [{:size 2}
    {:size 20 :fill "accent-fg"}
    {:size 2}
    {:size 1 :fill "extra-fg"}
    {:size 6 :fill "detail-fg"}
    {:size 1}]
   [{:size 2}
    {:size 8 :fill "accent-fg"}
    {:size 2}
    {:size 4 :fill "extra-fg"}
    {:size 10 :fill "symbols-fg"}
    {:size 1}]
   [{:size 1 :fill "accent-2-fg"}]
   [][{:size 5 :fill "alert-fg" :background "selection-background-bg"} {:size 2 :fill "accent-2-fg"}]
   [{:size 2}
    {:size 20 :fill "accent-fg"}
    {:size 2}
    {:size 1 :fill "extra-fg"}
    {:size 6 :fill "detail-fg"}
    {:size 1}]
   [{:size 2}
    {:size 8 :fill "accent-fg"}
    {:size 2}
    {:size 4 :fill "extra-fg"}
    {:size 10 :fill "symbols-fg"}
    {:size 1}]
   [{:size 1 :fill "accent-2-fg"}]
   [][{:size 5 :fill "alert-fg" :background "selection-background-bg"} {:size 2 :fill "accent-2-fg"}]
   [{:size 2}
    {:size 20 :fill "accent-fg"}
    {:size 2}
    {:size 1 :fill "extra-fg"}
    {:size 6 :fill "detail-fg"}
    {:size 1}]
   [{:size 2}
    {:size 8 :fill "accent-fg"}
    {:size 2}
    {:size 4 :fill "extra-fg"}
    {:size 10 :fill "symbols-fg"}
    {:size 1}]
   [{:size 1 :fill "accent-2-fg"}]
   [][{:size 5 :fill "alert-fg" :background "selection-background-bg"} {:size 2 :fill "accent-2-fg"}]
   [{:size 2}
    {:size 20 :fill "accent-fg"}
    {:size 2}
    {:size 1 :fill "extra-fg"}
    {:size 6 :fill "detail-fg"}
    {:size 1}]
   [{:size 2}
    {:size 8 :fill "accent-fg"}
    {:size 2}
    {:size 4 :fill "extra-fg"}
    {:size 10 :fill "symbols-fg"}
    {:size 1}]
   [{:size 1 :fill "accent-2-fg"}]
   [][{:size 5 :fill "alert-fg" :background "selection-background-bg"} {:size 2 :fill "accent-2-fg"}]
   [{:size 2}
    {:size 20 :fill "accent-fg"}
    {:size 2}
    {:size 1 :fill "extra-fg"}
    {:size 6 :fill "detail-fg"}
    {:size 1}]
   [{:size 2}
    {:size 8 :fill "accent-fg"}
    {:size 2}
    {:size 4 :fill "extra-fg"}
    {:size 10 :fill "symbols-fg"}
    {:size 1}]
   [{:size 1 :fill "accent-2-fg"}]
   [][{:size 5 :fill "alert-fg" :background "selection-background-bg"} {:size 2 :fill "accent-2-fg"}]
   [{:size 2}
    {:size 20 :fill "accent-fg"}
    {:size 2}
    {:size 1 :fill "extra-fg"}
    {:size 6 :fill "detail-fg"}
    {:size 1}]
   [{:size 2}
    {:size 8 :fill "accent-fg"}
    {:size 2}
    {:size 4 :fill "extra-fg"}
    {:size 10 :fill "symbols-fg"}
    {:size 1}]
   [{:size 1 :fill "accent-2-fg"}]
   [][{:size 5 :fill "alert-fg" :background "selection-background-bg"} {:size 2 :fill "accent-2-fg"}]
   [{:size 2}
    {:size 20 :fill "accent-fg"}
    {:size 2}
    {:size 1 :fill "extra-fg"}
    {:size 6 :fill "detail-fg"}
    {:size 1}]
   [{:size 2}
    {:size 8 :fill "accent-fg"}
    {:size 2}
    {:size 4 :fill "extra-fg"}
    {:size 10 :fill "symbols-fg"}
    {:size 1}]
   [{:size 1 :fill "accent-2-fg"}]
   [][{:size 5 :fill "alert-fg" :background "selection-background-bg"} {:size 2 :fill "accent-2-fg"}]
   [{:size 2}
    {:size 20 :fill "accent-fg"}
    {:size 2}
    {:size 1 :fill "extra-fg"}
    {:size 6 :fill "detail-fg"}
    {:size 1}]
   [{:size 2}
    {:size 8 :fill "accent-fg"}
    {:size 2}
    {:size 4 :fill "extra-fg"}
    {:size 10 :fill "symbols-fg"}
    {:size 1}]
   [{:size 1 :fill "accent-2-fg"}]
   [][{:size 5 :fill "alert-fg" :background "selection-background-bg"} {:size 2 :fill "accent-2-fg"}]
   [{:size 2}
    {:size 20 :fill "accent-fg"}
    {:size 2}
    {:size 1 :fill "extra-fg"}
    {:size 6 :fill "detail-fg"}
    {:size 1}]
   [{:size 2}
    {:size 8 :fill "accent-fg"}
    {:size 2}
    {:size 4 :fill "extra-fg"}
    {:size 10 :fill "symbols-fg"}
    {:size 1}]
   [{:size 1 :fill "accent-2-fg"}]
   []]
  )

(def dt (str/split-lines (slurp "resources/main.css")))

(defn text [times] (repeat times dt))

(with-redefs [v-c/text+style->ui text+style->ui]
  (crit/with-progress-reporting (crit/bench (v-c/transduce-text-style dt style-grid) :verbose))
  (crit/with-progress-reporting (crit/bench (doall (mapcat-reduce-match-style dt style-grid)) :verbose))
  (crit/with-progress-reporting (crit/bench (iter-loop-reduce-match-style dt style-grid) :verbose))
  (crit/with-progress-reporting (crit/bench (v-c/highlight-off dt style-grid) :verbose)))
