(ns vici.logic.ui-test
  (:require [clojure.test :refer :all]
            [clojure.test.check :refer [quick-check]]
            [clojure.test.check.clojure-test :refer [defspec assert-check]]
            [clojure.test.check.generators :as gen]
            [clojure.test.check.properties :as prop]
            [taoensso.timbre :as timbre]
            [vici.logic.ui :as l-ui]
            [fudje.sweet :refer :all]
            [fudje.core :refer [mocking]]))

(defn and-
  ([] true)
  ([a] (and- (and-) a))
  ([a b] (and a b)))

(defn or-
  ([] true)
  ([a] (or- (or-) a))
  ([a b] (or a b)))

(defn any [pred lst]
  (transduce (map pred) or- lst))

(defn all [pred lst]
  (transduce (map pred) and- lst))

(defn upd-gen [max-sz max-line]
  (gen/fmap (fn [[pos line]] {:pos pos :line line})
        (gen/tuple (gen/choose 0 max-sz) (gen/choose 0 max-line))))

(defn adj-pos [cur-pos limit size]
  (let [end-pos (+ cur-pos size)]
    (if (> end-pos limit)
      (- cur-pos (- end-pos limit))
      cur-pos)))

(defn gen-random-size-text [sz]
  (gen/bind (gen/choose 0 sz)
            (fn [i]
              (gen/resize i gen/string-ascii))) )

(defn text-upd-gen [max-sz max-line]
  (let [sz (dec max-sz)
        ln (dec max-line)]
    (gen/fmap (fn [[upd text]] (-> upd
                                   (assoc :size (count text))
                                   (update :pos adj-pos sz (count text))
                                   (assoc :content text)))
              (gen/tuple (upd-gen sz ln) (gen-random-size-text sz)))))

(def gen-color (gen/choose 0 16777215))

(def gen-style (gen/fmap (fn [[fill bg]] {:fill fill :background bg}) (gen/tuple gen-color gen-color)))

(defn stl-upd-gen [max-sz max-line]
  (let [sz (dec max-sz)
        ln (dec max-line)] (gen/fmap (fn [[upd size style]] (-> upd
                                                                (assoc :size size)
                                                                (update :pos adj-pos sz size)
                                                                (merge style)))
                                     (gen/tuple
                                       (upd-gen sz ln)
                                       (gen/choose 1 sz)
                                       gen-style))))

(defn blank [w h] (mapv (fn [_] (apply str (repeat w " "))) (range h)))

(defn recheck-property [property]
  (fn [seed]
    (assert-check (quick-check 1 property :seed seed))))

(defn empty-style [w h]
  (mapv (fn [i] [{:size w :pos 0 :fill 0 :background 0 :line i}]) (range h)))

(def text-update-property
  (prop/for-all [upd (text-upd-gen 100 30)]
                (let [updt (l-ui/update-text-grid (blank 100 30) [upd])
                      epos (+ (:pos upd) (:size upd))
                      line (nth updt (:line upd))
                      ss   (subs line (:pos upd) epos)]
                  (= (:content upd) ss))))

(defn xform-line [new-l old-l]
  (into [] (l-ui/up-style (map #(assoc % :- :x) new-l)) old-l))

(deftest all-previous-failures
  (testing "text update" (all (recheck-property text-update-property) [1528820282020
                                                                       1528820399975])))

(deftest text-update
  (testing "result"
    (is (= (l-ui/update-text-grid (blank 10 2)
                                  [{:size 5 :pos 0 :line 0 :content "Hello"}
                                   {:size 5 :pos 5 :line 1 :content "World"}])
           ["Hello     "
            "     World"]))))


(deftest single-style-update
  (comment "Single styles - Mirrors the initial redraw"
           "the three cases below are represented in numbers here"

           "|=========================|"
           "|111=========22222=====333|")

  (timbre/debug {::test-case "|111=======...|"})
  (fact "we can insert in the beginning of single style"
        (xform-line [{:size 10 :pos 0}]
                    [{:size 100 :pos 0}]) => [{:size 10 :pos 0 :- :x}
                                              {:size 90 :pos 10}])

  (timbre/debug {::test-case "|...===222===...|"})
  (fact "we can insert in the middle of single style"
        (xform-line [{:size 10 :pos 10}]
                    [{:size 100 :pos 0}]) => [{:size 10 :pos 0}
                                              {:size 10 :pos 10 :- :x}
                                              {:size 80 :pos 20}])

  (timbre/debug {::test-case "|...=========333|"})
  (fact "we can insert in the end of single style"
        (xform-line [{:size 10 :pos 90}]
                    [{:size 100 :pos 0}]) => [{:size 90 :pos 0}
                                              {:size 10 :pos 90 :- :x}])

  (comment "This is what actually hapens in the real redrawing workflow")

  (timbre/debug {::test-case "|=======|+++====...|"})
  (fact "we can skip a style and insert in the beginning of the second"
        (xform-line [{:size 10 :pos 10}]
                    [{:size 10 :pos 0}
                     {:size 90 :pos 10}]) => [{:size 10 :pos 0}
                                              {:size 10 :pos 10 :- :x}
                                              {:size 80 :pos 20}])

  (timbre/debug {::test-case "|====+++|+++====...|"})
  (fact "we can insert a style between two styles"
        (xform-line [{:size 10 :pos 5}]
                    [{:size 10 :pos 0}
                     {:size 90 :pos 10}]) => [{:size 5 :pos 0}
                                              {:size 10 :pos 5 :- :x}
                                              {:size 85 :pos 15}])

  (timbre/debug {::test-case "|=====|++++|=====...|"})
  (fact "we can replace an entire item"
        (xform-line [{:size 10 :pos 10}]
                    [{:size 10 :pos 0}
                     {:size 10 :pos 10}
                     {:size 80 :pos 20}]) => [{:size 10 :pos 0}
                                              {:size 10 :pos 10 :- :x}
                                              {:size 80 :pos 20}])

  (timbre/debug {::test-case "|===++|++++|++===...|"})
  (fact "we can replace an entire item"
        (xform-line [{:size 14 :pos 8}]
                    [{:size 10 :pos 0}
                     {:size 10 :pos 10}
                     {:size 80 :pos 20}]) => [{:size 8 :pos 0}
                                              {:size 14 :pos 8 :- :x}
                                              {:size 78 :pos 22}]))

(deftest multi-style-update
  (timbre/debug "Now things start to get scary")

  (timbre/debug {::test-case "|===+++==+++===...|"})
  (fact "we can add two elements inside one"
        (xform-line [{:size 2 :pos 8}
                     {:size 2 :pos 12}]
                    [{:size 20 :pos 0}]) => [{:size 8 :pos 0}
                                             {:size 2 :pos 8 :- :x}
                                             {:size 2 :pos 10}
                                             {:size 2 :pos 12 :- :x}
                                             {:size 6 :pos 14}])

  (timbre/debug {::test-case "|+++===========+++|"})
  (fact "we can add two elements inside one"
        (xform-line [{:size 2 :pos 0}
                     {:size 2 :pos 18}]
                    [{:size 20 :pos 0}]) => [{:size 2 :pos 0 :- :x}
                                             {:size 16 :pos 2}
                                             {:size 2 :pos 18 :- :x}])

  (timbre/debug {::test-case "|+++=====|=====+++|"})
  (fact "we can add two elements inside one"
        (xform-line [{:size 2 :pos 0}
                     {:size 2 :pos 18}]
                    [{:size 10 :pos 0}
                     {:size 10 :pos 10}]) => [{:size 2 :pos 0 :- :x}
                                              {:size 8 :pos 2}
                                              {:size 8 :pos 10}
                                              {:size 2 :pos 18 :- :x}])
  )



(defspec text-gets-updated 30 text-update-property)
