(defproject vici.core "0.1.0-SNAPSHOT"
  :plugins [[lein-tools-deps "0.4.1"]]
  :middleware [lein-tools-deps.plugin/resolve-dependencies-with-deps-edn]
  :lein-tools-deps/config {:config-files [:install :user :project]
                           :clojure-executables ["/usr/bin/clojure"]}
  :aot [vici.ui]
  :main vici.core
  )
