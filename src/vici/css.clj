(ns vici.css
  (:require [garden.core :as garden]))

(def styles
  {:base-16 {:background "#181818"
             :lighter-background "#282828"
             :selection-background "#383838"
             :comment "#585858"
             :dark-foreground "#b8b8b8"
             :default-foreground "#d8d8d8"
             :light-foreground "#e8e8e8"
             :light-background "#f8f8f8"
             :error "#ab4642"
             :warning "#dc9656"
             :alert "#f7ca88"
             :detail "#a1b56c"
             :border "#a1b56c"
             :extra "#86c1b9"
             :symbols "#7cafc2"
             :accent-2 "#ba8baf"
             :accent "#a16946"}
   :nord {:background "#2e3440"
          :lighter-background "#3b4252"
          :selection-background "#434c5e"
          :comment "#4c566a"
          :dark-foreground "#d8dee9"
          :default-foreground "#e5e9f0"
          :light-foreground "#eceff4"
          :light-background "#eceff4"
          :error "#bf616a"
          :warning "#d08770"
          :alert "#ebcb8b"
          :detail "#88c0d0"
          :border "#8fbcbb"
          :extra "#81a1c1"
          :symbols "#5e81ac"
          :accent "#a3be8c"
          :accent-2 "#b48ead"}})

(defn gen-css []
  (run! (fn [[style colors]]
          (garden/css {:output-to (str "resources/" (name style) ".css")}
                      (apply conj [[".root" {:-fx {:background-color (:background colors)
                                             :font "13pt 'Iosevka'" }}]
                             [".omni" {:-fx {:background-color (:lighter-background colors)
                                             :font "10pt 'Iosevka'"
                                             :border-color (:accent-2 colors)}}]
                             [".keyword" {:-fx {:fill (:detail colors)
                                                :weight :bold}}]
                             [".arrow" {:-fx {:fill (:symbols colors)}}]
                             [".help-text-suspendable" {:-fx {:fill (:extra colors)}}]
                             [".dim-text" {:-fx {:fill (:dark-foreground colors)}}]
                             [".help-text" {:-fx {:fill (:light-foreground colors)}}]]
                             (mapcat (fn [[kw color]]
                                       [[(str "." (name kw) "-fg") {:-fx {:fill color}}]
                                        [(str "." (name kw) "-bg") {:-fx {:background-color color}}]])
                                     colors))))
        styles))
