(ns vici.core
  (:require [vici.css :as css]
            [clojure.edn :as edn]
            [taoensso.timbre :as timbre]))

(defn vici-logger-output-fn
  ([     data] (vici-logger-output-fn nil data))
  ([opts data] ; For partials
   (let [{:keys [no-stacktrace? stacktrace-fonts]} opts
         {:keys [level ?err #_vargs msg_ ?ns-str ?file hostname_
                 timestamp_ ?line]} data
         msg (force msg_)
         excp? (:exception msg)]
     (cond-> {level [(keyword ?ns-str) ?line]}
       (nil? excp?) (assoc :- (edn/read-string msg))
       (some? excp?) (assoc :exception (merge (ex-data excp?)
                                              {:message (.getMessage excp?)}))))))

(timbre/refer-timbre)
(timbre/merge-config! {:output-fn vici-logger-output-fn
                       :ns-whitelist #{"vici.logic.ui"}})

(defn -main [& args]
  (case (first args)
    "css" (css/gen-css)
    "app" (javafx.application.Application/launch vici.ui (into-array String args))
    nil)
  (System/exit 0))
