(ns vici.components
  (:require [com.stuartsierra.component :as component]
            [taoensso.timbre :as timbre]
            [vici.components.neovim :as c-neovim]
            [vici.components.state :as c-state]
            [vici.components.config :as c-cfg]))

(def component-map
  (component/system-map
    :config (c-cfg/config)
    :state  (component/using (c-state/state) [:config])
    :neovim (component/using (c-neovim/nvim) [:config])))

(defn start []
  (timbre/debug {:event ::starating-component})
  (timbre/spy (component/start-system component-map)))
