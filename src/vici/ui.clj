(ns vici.ui
  (:require [fn-fx.fx-dom :as dom]
            [clojure.core.async :as a]
            [vici.ui.controls :as c]
            [vici.logic.ui :as l-ui]
            [vici.components :as components]
            [vici.protocols.state :as p-state]
            [vici.protocols.editor :as p-ed]
            [vici.ui.state :as s]
            [vici.ui.event :as e]
            [taoensso.timbre :as timbre])
  (:gen-class
    :extends javafx.application.Application))

(defn handler-fn [state event]
  (try
    (swap! state e/handle-event event)
    (catch Throwable ex
      (timbre/error {:exception ex}))))

(defn debug-channel [incoming-msg]
  (timbre/debug {:new-msg incoming-msg})
    true)

(defn upd [state grid-chan]
  (a/go-loop [upd-grid (a/<! grid-chan)]
             (swap! state assoc-in [::s/runtime ::s/panels 0] upd-grid)
             (Thread/sleep 3000)
             (recur (a/<! grid-chan))))

;; Can probably add the app and root stage from parameter
(defn -start [_ _]
  (let [components (components/start)
        ui-state (agent (dom/app (-> components :state :state deref c/stage) (partial handler-fn (-> components :state :state))))
        initial-grid (-> components :state :state deref ::s/runtime ::s/panels first ::s/text)
        msg [:nvim_ui_attach [(count (first initial-grid)) (count initial-grid) {}]]
        grid-chan (a/chan 8 (l-ui/xform-grids (-> components :state :state) [::s/runtime ::s/panels 0]))
        _ (upd (-> components :state :state) grid-chan)
        _ (a/>!! (-> components :neovim :write-chan) msg)
        _ (p-ed/connect (:neovim components) :notify-chan grid-chan)
        _ (p-ed/connect (:neovim components) :read-chan debug-channel)
        watch-ui (fn [_ _ _ state]
                   (if (-> state ::s/control ::s/close)
                     (javafx.application.Platform/exit)
                     (send ui-state
                           (fn [old-ui]
                             (let [begin (System/nanoTime)
                                   new-ui (c/stage state)
                                   produce (- (System/nanoTime) begin)
                                   new-dom (dom/update-app old-ui new-ui)
                                   refresh (- (System/nanoTime) begin produce)
                                   _ (timbre/info {:refresh (str (/ produce 1000M) "µs")
                                                    :draw (str (/ refresh 1000000M) "ms")})]
                               new-dom)))))
        ]
    (p-state/watch (:state components) :ui watch-ui)))
