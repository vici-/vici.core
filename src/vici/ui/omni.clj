(ns vici.ui.omni
  (:require [vici.ui.state :as s]
            [taoensso.timbre :as timbre]))

(defn suspend-to [state next-suspend]
  (if (some? next-suspend)
    (update-in state [::s/control ::s/suspended] conj next-suspend)
    (assoc-in state [::s/control ::s/suspended] [])))

(defn fn-suspend-to [next-suspend]
  (fn [state] (suspend-to state next-suspend)))

(defn fn-suspend-with
  ([action] (fn-suspend-with action nil))
  ([action next-suspend] (fn [state] (-> state
                                         action
                                         (suspend-to next-suspend)))))

(defn suspendable-action [kw help & args]
  (apply sorted-map
         ::help help
         ::action (fn-suspend-to kw)
         ::suspendable? true
         args))

(defn action [help action]
  {::help help
   ::suspendable? false
   ::action (fn-suspend-with action)})

(defn set-state [ks v]
  (fn [state]
    (assoc-in state ks v)))

(defn update-state [ks v]
  (fn [state]
    (update-in state ks v)))

(defn debug [state]
  (timbre/debug {:text (-> state ::s/runtime ::s/panels first ::s/text)})
  state)

(def actions
  {:ctrl-space (suspendable-action :ctrl-space "Omni mode - Perform actions that affect the editor"
                                   :a (suspendable-action :a "Change to app mode"
                                                          :d (action "Print text staet for debug" debug)
                                                          :h (action "Toggle highlight" (update-state [::s/options ::s/highlight?] not))
                                                          :n (action "Change css to nord" (set-state [::s/options ::s/stylesheets] ["nord.css"]))
                                                          :b (action "Change css to base-16" (set-state [::s/options ::s/stylesheets] ["base-16.css"]))
                                                          :x (action "Close vici" #(assoc-in % [::s/control ::s/close] true)))
                                   :t (action "Adds text" #(update-in % [::s/items] conj {::s/text "heuaheua"})))})
