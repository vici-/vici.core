(ns vici.ui.controls
  (:require [vici.ui.omni :as o]
            [vici.ui.state :as s]
            [taoensso.timbre :as timbre]
            [clojure.string :as str]
            [clojure.core.async :as async]
            [fn-fx.diff :refer [defui render]]
            [clojure.math.numeric-tower :as math]
            [fn-fx.controls :as ui])
  (:import (javafx.scene.paint Color)))

(def color-cache (atom {}))

(defn- to-fx-color [color]
  (when color
    (or (get @color-cache color)
        (-> color
            (Color/web)
            (->> (swap! color-cache assoc color))
            (get color)))))

(defn text+style->ui [text style]
  (let [fill (-> style :fill to-fx-color)
        bg (-> style :background to-fx-color)]
    (ui/text :text text :fill fill)))

(defn xform-line [text]
  (fn [xf]
    (let [line (volatile! text)]
      (fn
        ([] (xf))
        ([result] (if (> (count @line) 0)
                    (xf (xf result (ui/text :text @line)) (ui/text :text "\n"))
                    (xf result (ui/text :text "\n"))))
        ([result item]
         (let [l (deref line)
               sz (count l)
               selected (min (math/abs (:size item)) sz)
               txt-chunk (subs l 0 selected)
               product (text+style->ui txt-chunk item)]
           (vswap! line subs selected)
           (xf result product)))))))

(defn process-lines []
  (fn [xf]
    (fn
      ([] (xf))
      ([result] (xf result))
      ([result [text-line style-line]]
       (xf result (into [] (xform-line text-line) style-line))))))

(defn zip-with [coll]
  (fn [xf]
    (let [vcoll (volatile! coll)]
      (fn
        ([] (xf))
        ([result] (xf result))
        ([result item]
         (if-let [curr (first @vcoll)]
           (do (vswap! vcoll rest)
               (xf result [curr item]))
           (xf result)))))))

(defn transduce-text-style [text style]
  (into [] (comp (zip-with text) (process-lines) cat) style))

(defn highlight-off [txt _]
  (map #(ui/text :text %) (interpose "\n" txt)))

(def select-text-style-matcher {true transduce-text-style false highlight-off})

(defui OmniHelpItem
  (render [this {:keys [kw help suspendable?]}]
          (ui/text-flow
            :padding (ui/insets :top 10 :right 20 :bottom 10 :left 20)
            :children [(ui/text :text kw
                                :style-class ["keyword"])
                       (ui/text :text " → "
                                :style-class ["arrow"])
                       (ui/text :text (str (when suspendable? "+") help)
                                :style-class [(if suspendable? "help-text-suspendable" "help-text")])])))

(defui OmniBar
  (render [this state]
          (ui/grid-pane :alignment :center
                        :children [(ui/border-pane :style-class ["omni"]
                                                   :min-width 600
                                                   :max-width 1200
                                                   :left (ui/text :text (->> state ::s/control ::s/suspended (interpose " ") (apply str))
                                                                  :style-class ["dim-text"])
                                                   :right (ui/text :text (->> state ::s/control ::s/suspended (get-in o/actions) ::o/help)
                                                            :style-class ["help-text"]))])))

(defui OmniHelp
  (render [this state]
          (let [suspended-action (->> state
                                      ::s/control
                                      ::s/suspended
                                      (get-in o/actions))
                items (->> suspended-action
                           keys
                           (filter simple-keyword?)
                           (select-keys suspended-action)
                           (mapv (fn [[k {:keys [::o/help ::o/suspendable?]}]]
                                   (omni-help-item {:suspendable? suspendable? :kw (name k) :help help}))))]
            (ui/flow-pane :style-class ["omni"]
                          :max-height 20
                          :children items))))

(defui Board
  (render [this state]
          (ui/pane
            :children (let [highlight-fn (-> state ::s/options ::s/highlight? select-text-style-matcher)]
                        (mapv (fn [panel] (ui/text-flow :children (highlight-fn (::s/text panel)
                                                                                (::s/style panel))))
                              (-> state ::s/runtime ::s/panels))))) )


(defui Shell
  (render [this state]
          (let [omni? (-> state ::s/control ::s/suspended seq)]
            (ui/stack-pane
              :alignment :bottom-center
              :children (cond-> [(board state)]
                          omni? (conj (omni-help state) (omni-bar state)))))))


(defui Stage
  (render [this state]
          (ui/stage
            :title "vici"
            :on-close-request {:event :close}
            :shown true
            :scene (ui/scene
                     :stylesheets (->> state ::s/options ::s/stylesheets)
                     :on-key-pressed {:event :keypress
                                      :fn-fx/include {:fn-fx/event #{:code :control-down? :shift-down? :alt-down?}}}
                     :root (shell state)))))
