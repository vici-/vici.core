(ns vici.ui.adapter
  (:require [clojure.string :as str]))

(defn key-code->kw [kc ctrl? shift? alt?]
  (cond->> (str/lower-case (.getName kc))
      shift? (str/upper-case)
      true   (vector)
      alt?   (cons "alt")
      ctrl?  (cons "ctrl")
      true   (str/join "-")
      true   (keyword)))
