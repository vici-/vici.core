(ns vici.ui.event
  (:require [vici.ui.omni :as o]
            [vici.ui.state :as s]
            [vici.ui.adapter :as a]))

(defmulti handle-event (fn [state {:keys [event]}] event))

(defmethod handle-event :close [state _]
  (assoc-in state [::s/control ::s/close] true))

(defmethod handle-event :keypress [state event]
  (let [ctrl? (-> event :fn-fx/includes :fn-fx/event :control-down? boolean)
        shift? (-> event :fn-fx/includes :fn-fx/event :shift-down? boolean)
        alt? (-> event :fn-fx/includes :fn-fx/event :alt-down? boolean)
        code (-> event :fn-fx/includes :fn-fx/event :code)
        suspended-action (-> state ::s/control ::s/suspended)]
    (if-let [action (-> code
                        (a/key-code->kw ctrl? shift? alt?)
                        (#(get-in o/actions (conj suspended-action %)))
                        ::o/action)]
      (action state)
      (if (seq suspended-action)
        (o/suspend-to state nil)
        state))))
