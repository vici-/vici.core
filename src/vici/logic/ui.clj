(ns vici.logic.ui
  (:require [taoensso.timbre :as timbre]
            [vici.ui.state :as s]
            [garden.color :as color]
            [clojure.string :as str]))

(defn rgb24->hex [irgb]
  (color/as-hex
    (color/as-color
      {:red (bit-and irgb 0xff)
       :green (bit-and (bit-shift-right irgb 8) 0xff)
       :blue (bit-and (bit-shift-right irgb 16) 0xff)})))

(defn to-hex [stl]
  (cond-> stl
  (some? (:fill stl)) (update :fill rgb24->hex)
  (some? (:background stl)) (update :background rgb24->hex)))

(defn update-line-at [old {:keys [content pos size line]}]
  (let [new-str (str (subs old 0 pos) content (subs old (+ pos size)))
        changed-piece (subs old pos (+ pos size))
        new-piece (subs new-str pos (+ pos size))]
    new-str))

(defn xform-lines [old-grid]
  (fn [xf]
    (let [v-grid (volatile! old-grid)]
      (fn
        ([] (xf))
        ([result] (reduce xf result @v-grid))
        ([result item]
         (vswap! v-grid update (:line item) update-line-at item)
         result))
      )))

(defn update-line [old-grid new-line]
  (reduce (fn [acc i] (update acc (:line i) update-line-at i)) old-grid new-line))

(comment
  ;; Begins after this element
  "===========|============|=============="
  "==========================*******======"

  ;; Begins and ends inside
  "===========|============|=============="
  "===========++*********+++=============="

  ;; Begins inside and ends outside
  "===========|============|=============="
  "===========++**************============"

  ;; Begins outside and ends inside
  "===========|============|=============="
  "=========*************+++=============="

  ;; Begins and ends outside
  "===========|============|=============="
  "=========******************============"
  )


(defn smaller-on-begin [curr next]
  (let [size (- (:pos next) (:pos curr))]
    (when (> size 0)
      (assoc curr :size size))))

(defn smaller-on-end [curr next]
  (let [end-of-next (+ (:pos next) (:size next))
        size (- (+ (:pos curr) (:size curr)) end-of-next)]
    (when (> size 0)
      (-> curr
          (assoc :size size)
          (assoc :pos end-of-next)))))

(defmacro tapd [label & form]
  `(do
     (timbre/debug {::tap ~label})
     ~@form))

(defn up-style [new-stl-line]
  (fn [xf]
    (let [stl (volatile! new-stl-line)]
      (fn consume-line
        ([] (xf))
        ([result] (if-let [stl (to-hex (first @stl))]
                    (xf result stl)
                    (xf result)))
        ([result item]
         (tapd {:item item})
         (if-let [next-stl (to-hex (first @stl))]
           (let [end-of-curr (+ (:pos item) (:size item))
                 end-of-next (+ (:pos next-stl) (:size next-stl))

                 next-begins-inside (>= (:pos next-stl) (:pos item))
                 next-ends-inside (>= end-of-curr end-of-next)]

             (cond
               (<= end-of-curr (:pos next-stl)) (tapd "next starts after current item"
                                                      (xf result item))

               (and next-begins-inside next-ends-inside) (tapd "next fits inside this item"
                                                               (let [head (smaller-on-begin item next-stl)
                                                                     last- (smaller-on-end item next-stl)]
                                                                 (vswap! stl rest)
                                                                 (cond-> result
                                                                   (some? head)  (xf head)
                                                                   true          (xf next-stl)
                                                                   (some? last-) (consume-line last-))))

               (and next-begins-inside (not next-ends-inside)) (tapd "next overflows the current item"
                                                                     (let [head (smaller-on-begin item next-stl)]
                                                                       (cond-> result
                                                                         (some? head) (xf head)
                                                                         true         (xf next-stl))))

               (and (not next-begins-inside) next-ends-inside) (tapd "next began in previous item is ongoing"
                                                                     (let [last- (smaller-on-end item next-stl)]
                                                                       (vswap! stl rest)
                                                                       (cond-> result
                                                                         (some? last-) (consume-line last-))))

               :default (tapd "unsure why would fall here but none of the above" result)))
           (tapd "if-let failed and no next style exists" (xf result item))))))))

(defn are-equal-styles [held next]
  (and
    (= (:fill held) (:fill next))
    (= (:background held) (:background next))))

(defn defrag []
  (fn [xf]
    (let [curr (volatile! ::none)]
      (fn
        ([] (xf))
        ([result] (xf result))
        ([result item]
         (when (#{::none} @curr)
           (vreset! curr item))
         (let [curr-item @curr]
           (cond
             (are-equal-styles curr-item item) (do (vswap! curr update :size + (:size item))
                                                   result)
             (and (= (:pos item) (:pos curr-item))
                  (>= (:size item) (:size curr-item))) (do (vreset! curr item)
                                                           result)
             :default (do (vreset! curr item)
                          (if (pos? (:size curr-item))
                            (xf result curr-item)
                            result)))))))))

(defn tap [label]
  (fn [xf]
    (fn
      ([] (xf))
      ([r] (xf r))
      ([r i]
       (timbre/debug {label i})
       (xf r i)))))

(defn update-style-line [old-line new-line]
  (into [] (comp (up-style new-line)  #_(defrag)) old-line))

(defn update-style-grid [old-grid update-req]
  (let [reqs (volatile! (sort-by :line update-req))]
    (loop [ix 0
           nlines old-grid
           updated []]
      (let [cur-reqs @reqs
            [this-line tail] (split-with #(-> % :line (= ix)) cur-reqs)
            this-line (sort-by :pos this-line)]
        (cond
          ;; No more updates left
          (nil? cur-reqs) (into updated nlines)

          ;; We're on a line that has an update style request
          (= (:line (first this-line)) ix) (do
                                            (vreset! reqs tail)
                                            (recur (inc ix)
                                                   (rest nlines)
                                                   (conj updated (update-style-line (first nlines) this-line))))

          ;; We don't have anything to do on this line
          (and (seq cur-reqs) (seq nlines)) (recur (inc ix) (rest nlines) (conj updated (first nlines)))

          ;; We're done
          :default updated)))))

(def update-text-grid (fn [old-grid new-grid]
                        (into [] (xform-lines old-grid) new-grid)))

(defn update-grids [old-grid new-grid]
  (let [new-text (update-text-grid (::s/text old-grid) (:text new-grid))
        new-style (update-style-grid (::s/style old-grid) (:style new-grid))]
    (timbre/debug {:old-style (nth (::s/style old-grid) 1) :new-style (nth new-style 1)})
    {::s/text new-text ::s/style new-style}))

(defn xform-grids [state lens]
  (fn [xf]
    (fn
      ([] (xf))
      ([result] (xf result))
      ([result item]
       (let [current (get-in @state lens)
             updated (update-grids current item)]
         (xf result updated))))))
