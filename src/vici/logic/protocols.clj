(ns vici.logic.protocols)

(defn idempotent [component kw side-effect]
  (if (kw component)
    component
    (assoc component kw (side-effect))))

(defn with [component & kw-eff]
  (if ((first kw-eff) component)
    component
    (merge component (apply hash-map kw-eff))))
