(ns vici.logic.ns)

(defn namespaced [new-ns mp]
  (some->> mp
       (mapcat (fn [[k v]] [(keyword (name new-ns) (name k)) v]))
       (apply hash-map)))
