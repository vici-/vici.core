(ns vici.components.config
    (:require [com.stuartsierra.component :as component]
              [taoensso.timbre :as timbre]
              [vici.protocols.config :as p-cfg]
              [vici.logic.ns :as l-ns]
              [vici.logic.protocols :refer [idempotent]]
              [vici.ui.state :as s]))

(defmulti handle-tag (fn [tag value] (-> tag name keyword)))

(defrecord Config [file data]
  component/Lifecycle

  (start [this]
    (timbre/debug {:event ::starting-config})
    (-> this (idempotent :data #(->> (or file (str (or (System/getenv "XDG_CONFIG")
                                                       (str (System/getenv "HOME") "/.config"))
                                                   "/vici/config.edn"))
                                     (slurp)
                                     (clojure.edn/read-string {:default handle-tag})
                                     (atom)))))

  (stop [this]
    (dissoc this :data))

  p-cfg/Config

  (from [this ks]
    (p-cfg/from this ks nil))

  (from [this ks default]
    (-> this
        :data
        deref
        (get-in ks default)))

  (as [this new-ns ks]
    (p-cfg/as this new-ns ks nil))

  (as [this new-ns ks default]
    (l-ns/namespaced new-ns (p-cfg/from this ks default))))

(defn config [] (map->Config {}))
