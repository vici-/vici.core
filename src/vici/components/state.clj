(ns vici.components.state
  (:require [com.stuartsierra.component :as component]
            [taoensso.timbre :as timbre]
            [vici.protocols.state :as p-state]
            [vici.logic.protocols :refer [idempotent]]
            [vici.protocols.config :as p-cfg]
            [vici.ui.state :as s]))

(defn white-line [sz]
 (apply str (doto (make-array Character/TYPE sz) (java.util.Arrays/fill \space))))

(defn blank-style [sz]
 [{:pos 0 :size sz :fill "#FFFFFF"}])

(defn style-grid [h w]
  (mapv (fn [ix]
          (assoc-in (blank-style w) [0 :line] ix)) (range h)))

(defn text-grid [h w]
  (vec (repeat h (white-line w))))

(defn default-state [h w]
  {::s/options {::s/stylesheets ["base-16.css"]
                ::s/highlight? true}
   ::s/control {::s/suspended []
                ::s/close false}
   ::s/runtime {::s/panels [{::s/text (text-grid h w)
                             ::s/style (style-grid h w)}]}})


(defn config->state [config]
  (let [shell (p-cfg/as config :vici.ui.state [:shell])]
    (cond-> {}
      (some? shell) (assoc-in [::s/options] shell))))

(defn init-state! [config]
  (fn []
    (let [[height width] (p-cfg/from config [:runtime :sizes] [100 200])]
      (timbre/debug {::init-window-with-sizes {:height height :width width}})
      (atom (merge-with merge (default-state height width) (config->state config))))))

(defrecord State [config]
  component/Lifecycle

  (start [this]
    (timbre/debug {:event ::starting-state})
    (-> this (idempotent :state (init-state! config))))

  (stop [this]
    (dissoc this :state))

  p-state/State

  (set-state [this kw v]
    (swap! (:state this) assoc-in kw v))

  (watch [this id watch-fn]
    (add-watch (:state this) id watch-fn))

  (unwatch [this id]
    (remove-watch (:state this) id)))

(defn state [] (map->State {}))
