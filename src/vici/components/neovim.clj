(ns vici.components.neovim
  (:require [me.raynes.conch.low-level :as sh]
            [msgpack.core :as msgpack]
            [taoensso.timbre :as timbre]
            [vici.logic.protocols :as l-p]
            [vici.ui.state :as s]
            [clojure.core.async :as a]
            [clojure.java.io :as io]
            [vici.protocols.editor :as p-ed]
            [com.stuartsierra.component :as component]))



;; Neovim protocol - will be outsourced
(def types
  {:request 0
   :respose 1
   :notify 2})

(defn write-msg [tp id op payload]
  [(tp types) id (name op) payload])

(defn format-out-msg [tp]
  (fn [xf]
    (let [ix (volatile! 0)
          type- (tp types)]
      (fn
        ([] (xf))
        ([result] (xf result))
        ([result item]
         (let [index @ix]
           (vswap! ix inc)
           (xf result (into [type- index] item))))))))

(defn nvim-stl->vici-stl [item]
  (clojure.set/rename-keys item {"bold" :bold
                                 "foreground" :fill
                                 "background" :background}))

(defn stream-redraw [redraw]
  (conj (last redraw) [::frame-end []]))

(defn nvim->vici []
  (fn [xf]
    (let [pos (volatile! [0 0])
          style (volatile! {})
          line-style (volatile! {})

          upd-pos! (fn [result coord]
                     (vreset! pos coord)
                     result)

          upd-stl! (fn [result kw arg]
                     (vswap! style assoc kw arg)
                     result)

          set-lstl! (fn [result arg]
                      (vreset! line-style (merge @style (nvim-stl->vici-stl arg)))
                      result)
          put! (fn [result args]
                 (let [sz (count args)
                       [ln clm] (mapv long @pos)
                       lstyle @line-style
                       coords {:size sz :pos clm :line ln}
                       line (apply str (flatten args))]
                   (vswap! pos update 1 + sz)
                   (timbre/debug (merge coords {:lstyle lstyle :content line}))
                   (cond-> result
                       (seq lstyle)        (xf [:style (merge lstyle coords)])
                       (pos? (count line)) (xf [:text (assoc coords :content line)]))))]
      (fn
        ([] (xf))
        ([result] (xf result))
        ([result [action & args]]
         (case action
           "update_fg"     (upd-stl! result :fill       (-> args first first))
           "update_bg"     (upd-stl! result :background (-> args first first))
           "highlight_set" (set-lstl! result            (-> args first first))
           "put"           (put! result args)
           "cursor_goto"   (upd-pos! result             (first args))
           ::frame-end     (xf result [::frame-end])
           result))))))

(defn tap [label]
  (fn [xf]
    (fn
      ([] (xf))
      ([r] (xf r))
      ([r i]
       (timbre/debug label)
       #_(timbre/debug {label i})
       (xf r i)))))

(defn vici-as-lines []
  (fn [xf]
    (let [style-line (volatile! [])
          text-line (volatile!  [])]
      (fn val-t
        ([] (xf))
        ([result] (xf result {:text @text-line :style @style-line}))
        ([result [kw item]]
         (case kw
           :text (do (vswap! text-line conj item)
                     result)
           :style (do (vswap! style-line conj item)
                      result)
           ::frame-end (val-t result)
           (xf result [kw item])))))))

;; Takes a [:nvim-eval]
(def write-xf (comp
          ;; Vici -> Neovim xf
          (map #(update % 0 name))
          (format-out-msg :request)))

(def ui-xf (comp (map stream-redraw)    ;; get only the payload
                 cat
                 (nvim->vici)           ;; Transform to the context of vici
                 (tap ::translated)
                 (vici-as-lines)        ;; group changes into lines
                 (tap ::output)
                 ))

(defn in-stream [proc dead-letter lock]
  (let [write-chan (a/chan 8 (comp write-xf (map msgpack/pack)))]
    (a/go-loop [out (a/<! write-chan)]
               (try
                 (sh/feed-from proc out)
                 (catch Exception e (a/<! dead-letter {:data out
                                                       :type :failed
                                                       :direction :out
                                                       :exception e})))
               (when (-> lock deref :write-stream-out)
                 (recur (a/<! write-chan))))
    write-chan))

(defn out-stream [proc dead-letter lock]
  (let [read-chan (a/chan 8)
        notify-chan (a/chan 8 ui-xf)]
    (a/go-loop [in (msgpack/unpack (:out proc))]
               (case (first in)
                 1 (a/>! read-chan in)
                 2 (a/>! notify-chan in)
                 (a/>! dead-letter {:data in
                                    :type :unknown
                                    :diraction :in}))
               (when (-> lock deref :read-stream-in)
                 (recur (msgpack/unpack (:out proc)))))
    [read-chan notify-chan]))

(defrecord Neovim [config]
  component/Lifecycle

  (start [this]
    (timbre/debug {:event ::starting-state})
    (let [neovim (sh/proc "nvim" "--embed" "/opt/code/vici/vici.core/src/vici/components/neovim.clj")
          lock (atom {:read-stream-in true
                      :write-stream-out true
                      :write-chan false
                      :read-chan false
                      :notify-chan false
                      :dead-letter false})
          dead-letter (a/chan 8)
          [read-chan notify-chan] (out-stream neovim dead-letter lock)]
      (-> this
          (l-p/with
            :instance neovim
            :write-chan (in-stream neovim dead-letter lock)
            :read-chan read-chan
            :notify-chan notify-chan
            :dead-letter dead-letter
            :lock lock))))

  (stop [this]
    (-> this :instance (sh/done))
    (dissoc this :instance)
    (dissoc this :write-chan))

  p-ed/Editor

  (connect [this kw handler]
    (timbre/debug {:connecting-pipe-to kw})
    (let [our-chan (-> this kw)]
      (if (fn? handler)
        (a/go-loop [msg (a/<! our-chan)]
                   (timbre/debug {:piping-msg-chan kw})
                   (when (and (handler msg) (-> this :lock deref kw))
                     (recur (a/<! our-chan))))
        (a/pipe our-chan handler)))
    (swap! (:lock this) assoc kw true))

  (disconnect [this kw]
    (swap! (:lock this) assoc kw false)))

(defn nvim []
  (map->Neovim {}))
